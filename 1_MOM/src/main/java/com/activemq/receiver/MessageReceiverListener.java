package com.activemq.receiver;
 
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
 
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
 
public class MessageReceiverListener implements MessageListener {
    // URL of the JMS server
    private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
    // default broker URL is : tcp://localhost:61616"
    // Name of the queue we will receive messages from
    private static String subject = "JCG_QUEUE";

    public static void main(String[] args) throws JMSException {
        // Getting JMS connection from the server
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(url);
        Connection connection = connectionFactory.createConnection();
    	try {
	        connection.start();	 
	        // Creating session for sending messages
	        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	        // Getting the queue 'JCG_QUEUE'
	        Destination destination = session.createQueue(subject);
	        // MessageConsumer is used for receiving (consuming) messages
	        MessageConsumer consumer = session.createConsumer(destination);
	        // Here we receive the message.
	        MessageListener listener = new MessageReceiverListener();
	        consumer.setMessageListener(listener);
        } catch (Exception e) {
            System.out.println("Caught exception!");
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (JMSException e) {
                    System.out.println("Could not close an open connection...");
                }
            }
        }
    }

	@Override
	public void onMessage(Message message) {
        // We will be using TestMessage in our example. MessageProducer sent us a TextMessage
        // so we must cast to it to get access to its .getText() method.
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;
            try {
				System.out.println("Received message '" + textMessage.getText() + "'");
			} catch (JMSException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
	}
}