package com.remote.rmi;

import java.rmi.*;
import java.rmi.server.*;

public class CalculatorImpl extends UnicastRemoteObject implements CalculatorInterface
{
	private static final long serialVersionUID = 1L;

	public CalculatorImpl() throws RemoteException {
		super();
	}

	@Override
	public long add(long a, long b) throws RemoteException {
		return a+b;
	}

	@Override
	public long sub(long a, long b) throws RemoteException {
		return a-b;
	}

	@Override
	public long mul(long a, long b) throws RemoteException {
		return a*b;
	}

	@Override
	public long div(long a, long b) throws RemoteException {
		return a/b;
	}
}