package com.remote.rmi;

import java.rmi.*;

public class CalculatorServer {
	public static void main(String[] args)
	{
		try {
			System.out.println( "Serveur : Construction de l’implémentation ");
			CalculatorImpl calc= new CalculatorImpl();
			System.out.println("Objet Reverse lié dans le RMIregistry");
			Naming.rebind("rmi://sinus.cnam.fr:1099/Calculator", calc);
			System.out.println("Attente des invocations des clients …");
		}
		catch (Exception e) {
			System.out.println("Erreur de liaison de l'objet Calculator");
			System.out.println(e.toString());
		}
	}
}