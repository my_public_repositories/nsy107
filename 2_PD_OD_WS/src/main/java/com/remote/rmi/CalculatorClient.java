package com.remote.rmi;

import java.rmi.*;

public class CalculatorClient
{
	public static void main (long [] args)
	{ 
		try{
			// System.setSecurityManager(new RMISecurityManager()); ???
			// RMISecurityManager is deprecated since java 1.8 in 2014 :(
			
			CalculatorInterface calc = (CalculatorInterface) Naming.lookup("rmi://sinus.cnam.fr:1099/Calculator");
			long result = calc.add(args [0], args [1]);
			System.out.println ("La somme de "+args[0]+" et "+args[1]+" est "+result);
			result = calc.sub(args [0], args [1]);
			System.out.println ("La différence de "+args[0]+" et "+args[1]+" est "+result);
			result = calc.mul(args [0], args [1]);
			System.out.println ("Le produit de "+args[0]+" et "+args[1]+" est "+result);
			result = calc.div(args [0], args [1]);
			System.out.println ("Le ratio de "+args[0]+" et "+args[1]+" est "+result);
		}
		catch (Exception e)
		{
			System.out.println ("Erreur d'accès à l'objet distant.");
			System.out.println (e.toString());
		} 
	}
}
